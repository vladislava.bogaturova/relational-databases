const express=require('express')
const userRouter = require('./routes/user.rotes')
const postRouter = require('./routes/post.rotes')

const PORT=process.env.PORT|| 8080

const app=express()

app.use(express.json())
app.use('/api',userRouter)
app.use('/api',postRouter)


app.listen(PORT,()=>console.log(`Server started on port ${PORT}`))