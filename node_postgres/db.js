const dotenv = require('dotenv');

dotenv.config();

const Pool= require('pg').Pool

const pool=new Pool({
    user:process.env.POSTGRESQL_USER,
    password:process.env.POSTGRESQL_PASSWORD,
    host:'localhost',
    port:5432,
    database:process.env.POSTGRESQL_DATABASE
})

module.exports=pool