# relational-databases

Database- is an organized collection of data stored and accessed electronically from a computer system. 

1979 year first release (commercial)

The relational model:
    - Codd's 12 rules (actually 13)
        0. foundation rule
        1. information rule
        2. guaranteed access rule
        3. systematic treatment of Null Values
        4. Dynamic online catalog based on relational model
        5. Comprehensive data sublanguage rule
        6. View updating rule
        7. High level insert, update and delete
        8. Physical data independence
        9. Logical data independence
        10.Integrity independence
        11.Distribution independence
        12.Nonsubversion rule

 Data modeling:
    -conceptual data model
    -logical data model
    -physical data model  


 ER (Entity relationship) modeling






